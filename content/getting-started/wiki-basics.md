---
eleventyNavigation:
  key: WikiBasics
  title: Contributing to Wikis
  parent: GettingStarted
  order: 50
  draft: true
---

The term wikiwiki comes from Hawaii and means 'fast' or 'with speed'. Wiki is a collaborative space on the Web that is usually edited online. It is a common practice to use Wikis to collect knowledge and share information. 

You can edit Wikis in plain text markdown and preview it.

In the settings for your project, under 'Advanced Settings' there is a choice where you can use Built-in Wiki or provide URL for external one if you already have it hosted somewhere else. You can also completely disable it if you don't need it.

You can read about how to include images in wiki pages [here](https://docs.codeberg.org/advanced/images-in-wiki-pages/).